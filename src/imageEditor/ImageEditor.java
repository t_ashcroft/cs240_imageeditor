package imageEditor;

public class ImageEditor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//  PUT ALL THIS IN A TRY/CATCH BLOCK!
		
//		String input = args[0];
//		String output = args[1];
//		String transform = args[2];
		
		String input = "./in.ppm";
		String output = "./out.ppm";
		String transform = "invert";
		
		Image image = new Image(input, output);

		if(args.length > 3)
		{
			try
			{
				int blur_width = Integer.parseInt(args[3]);	
				if(blur_width <= 0)
				{
					throw new Exception("Blur size must be greater than 0.");
				}
			}
			catch(Exception e)
			{
				System.out.println("The given argument was not a valid number.");
				return;
			}
		}
		
		switch (transform)
		{
		case "invert": image.invertImage();
		break;
		case "grayscale": image.grayScale();
		break;
		case "emboss": image.embossImage();
		break;
		case "motionblur": image.blurImage();
		break;
		default:
	//		throw new Exception();
		}
		
		image.saveImage();
		return;
		
	}

}
