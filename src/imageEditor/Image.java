package imageEditor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Vector;

public class Image {

	private Pixel[][] image;
	private int height;
	private int width;
	private int blur_width;
	String input;
	String output;
	
	public Image(String input, String output)
	{
		this.input = input;
		this.output = output;
		parseImage();
		image = new Pixel[height][width];
	}
	
	public Image(String input, String output, int width)
	{
		this.input = input;
		this.output = output;
		blur_width = width;
		parseImage();
		image = new Pixel[height][width];
	}
	
	private void parseImage()
	{
		Vector<Pixel> img = new Vector<Pixel>();
		File file = new File(input);
		Scanner scan;
		try {
			scan = new Scanner(file);
			scan.useDelimiter("\\s*");
		
			//*************NEEDS TO CATCH COMMENTS**************
			String magic = scan.next(); // gets the P3
			width = scan.nextInt(); // gets the width value
			System.out.println("pause");
			System.out.println(scan.nextInt());
			height = scan.nextInt(); // gets the height value
			int max_color = scan.nextInt(); // this value is 255 for this lab.
			// ************NEEDS TO CATCH COMMENTS**************
			
			
			while(scan.next() != null)
			{
				String temp = scan.next();
				if(temp.startsWith("#"))
				{
					temp = scan.nextLine();
				}
				else
				{
					Pixel temp_pixel = new Pixel();
					temp_pixel.setRed(scan.nextInt());
					temp_pixel.setGreen(scan.nextInt());
					temp_pixel.setBlue(scan.nextInt());
					img.add(temp_pixel);
				}
			}
			
			
			for(int i = 0; i < height; i++)
			{
				for(int j = 0; j < width; j++)
				{
					image[i][j] = img.get(0);
					img.remove(0);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void grayScale()
	{
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				image[i][j].avgValues();
			}
		}
	}
	
	public void invertImage()
	{
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				image[i][j].invertValues();
			}
		}
	}
	
	public void embossImage()
	{
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				if(i == 0 || j == 0)
				{
					image[i][j].emboss(0, 0, 0, true);
				}
				else
				{
					image[i][j].emboss(image[i-1][j-1].getRed(), image[i-1][j-1].getGreen(), image[i-1][j-1].getBlue(), false);
				}
			}
		}
	}
	
	public void blurImage()
	{
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				blurPixels(i, j);
			}
		}
	}
	
	private void blurPixels(int row, int col)
	{
		Vector<Integer> redVals = new Vector<Integer>();
		Vector<Integer> greenVals = new Vector<Integer>();
		Vector<Integer> blueVals = new Vector<Integer>();
		
		// get current pixel
		redVals.add(new Integer(image[row][col].getRed()));
		greenVals.add(new Integer(image[row][col].getGreen()));
		blueVals.add(new Integer(image[row][col].getBlue()));
		
		// get pixels blur_width - 1 beyond current pixel.
		for(int i = 0; i < blur_width; i++)
		{
			if(row + blur_width - i < (width - 1))
			{
				redVals.addElement(new Integer(image[row+blur_width-i][col].getRed()));
				greenVals.addElement(new Integer(image[row+blur_width-i][col].getGreen()));
				blueVals.addElement(new Integer(image[row+blur_width-i][col].getBlue()));
			
			}
		}
		
		
	}
	
	public void saveImage()
	{
		try {
			PrintWriter out = new PrintWriter(output);
			out.write("P3\n");
			out.write(width + "\n");
			out.write(height + "\n");
			out.write("255\n");
			
			for(int i = 0; i < height; i++)
			{
				for(int j = 0; j < width; j++)
				{
					out.write(image[i][j].getRed() + "\n");
					out.write(image[i][j].getGreen() + "\n");
					out.write(image[i][j].getBlue() + "\n");
				}
			}
			
			out.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
