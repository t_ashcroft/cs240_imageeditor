package imageEditor;

public class Pixel {

	private int red;
	private int blue;
	private int green;
	
	public Pixel()
	{
		red = 0;
		green = 0;
		blue = 0;
	}
	
	public Pixel(int r, int g, int b)
	{
		red = r;
		green = g;
		blue = b;
	}
	
	public void setRed(int r)
	{
		red = r;
	}
	
	public void setGreen(int g)
	{
		green = g;
	}
	
	public void setBlue(int b)
	{
		blue = b;
	}
	
	public int getRed()
	{
		return red;
	}
	
	public int getGreen()
	{
		return green;
	}
	
	public int getBlue()
	{
		return blue;
	}
	
	public void avgValues()
	{
		int temp = red + green + blue;
		temp = temp / 3;
		red = temp;
		green = temp;
		blue = temp;
	}
	
	public void invertValues()
	{
		int max = 255;
		red = max - red;
		green = max - green;
		blue = max - blue;
	}
	
	public void emboss(int ULRed, int ULGreen, int ULBlue, boolean OFB)
	{
		// calculate differences from upper left neighbor pixel.
		red = red - ULRed;
		green = green - ULGreen;
		blue = blue - ULBlue;
		int maxDifference = 300;
		
		// find the greatest difference
		if((red >= green && red >= blue) || (-red >= green && -red >= blue))
		{
			maxDifference = red;
		}
		else if((green >= red && green >= blue) || (-green >= red && -green >= blue))
		{
			maxDifference = green;
		}
		else if((blue >= red && blue >= green) || (-blue >= red && -blue >= green))
		{
			maxDifference = blue;
		}
		
		int change_factor = 128 + maxDifference;
		
		if(OFB)
		{
			change_factor = 128;
		}
		else if(change_factor < 0)
		{
			change_factor = 0;
		}
		else if(change_factor > 255)
		{
			change_factor = 255;
		}
		
		red = change_factor;
		green = change_factor;
		blue = change_factor;		
	
	}
		
}
